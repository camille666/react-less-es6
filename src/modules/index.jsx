import React from 'react'
import { NavLink, Route, Switch } from 'react-router-dom'
import InitPage from '../components/initPage/initPage'

export default class HomePage extends React.Component {
    render() {
        return (
            <div>
                <NavLink to="/initpage">初始化</NavLink>
                <Switch>
                    <Route exact={true} path="/initpage" component={InitPage} />
                </Switch>
            </div>
        )
    }
}
